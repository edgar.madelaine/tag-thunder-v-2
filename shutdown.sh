#!/bin/bash

# Exécuter screen -ls et extraire les noms de session
sessions=$(sudo screen -ls | grep -oP '\d+\.\K\S+')

# Boucler à travers chaque nom de session
for session in $sessions; do
    echo "Arrêt de la session $session"
    # Arrêter la session Screen
    sudo screen -S $session -X quit
done

# Récupérer les PID des processus utilisant le port 80
pids_port_80=$(sudo lsof -t -i:80)

# Récupérer les PID des processus utilisant le port 443
pids_port_443=$(sudo lsof -t -i:443)

# Tuer les processus utilisant le port 80
for pid in $pids_port_80; do
    echo "Arrêt du processus PID $pid utilisant le port 80"
    sudo kill $pid
done

# Tuer les processus utilisant le port 443
for pid in $pids_port_443; do
    echo "Arrêt du processus PID $pid utilisant le port 443"
    sudo kill $pid
done
# Relancer le daemon pour appliquer les modifications de configuration
sudo systemctl daemon-reload
sudo systemctl restart docker
echo "Docker prêt"
