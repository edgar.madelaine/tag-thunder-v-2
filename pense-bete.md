OS du serveur : Linux ktagthunder-dev 6.1.0-18-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.76-1 (2024-02-01) x86_64 GNU/Linux

# INSTALLATION DE CONDA
installer la première version linuxe à l'url https://docs.anaconda.com/free/miniconda/ et l'ajouter au serveur
- bash sftp://bournon241@ktagthunder-dev.unicaen.fr/home/bournon241/Miniconda3-latest-Linux-x86_64.sh
- source ~/.bashrc

# INSTALLATION DU PROJET
- git clone https://git.unicaen.fr/edgar.madelaine/tag-thunder-v-2.git
- cd tag-thunder-v-2/tagthunder/javascript/html-augmentation
- npm pack
- cd ../../..
- poetry lock --no-update
- (sudo usermod -aG docker bournon241)
- (sudo chmod 666 /var/run/docker.sock)
- (sudo apt update)
- (sudo apt install apparmor)
- (sudo apt install ffmpeg)
- nano tagthunder/javascript/puppeteer-crawler/Dockerfile
	- ENV http_proxy="http://proxy.unicaen.fr:3128"
	- ENV https_proxy="http://proxy.unicaen.fr:3128"
- (docker system prune -a)
- sudo systemctl start docker
- (sudo systemctl status docker)
- make install

# CREATION DE L'ENVIRONNEMENT POUR TAGTHUNDER
- conda create -n tagthunder-env python=3.9
- conda activate tagthunder-env

# COMMENT SE CONNECTER AU SITE (ne fonctionne pas en réseau publique)
- 10.14.176.49
- http://ktagthunder-dev.unicaen.fr 
- https://ktagthunder-dev.unicaen.fr

# PROXY
https://faq-etu.unicaen.fr/proxy
- export http_proxy="http://proxy.unicaen.fr:3128"
- export https_proxy="http://proxy.unicaen.fr:3128"

## Vérifier la configuration actuelle du proxy avec la commande :
- env | grep -i proxy

# CRÉATION D'UN LIEN SYMBOLIQUE VERS 'poetry'
- sudo ln -s /path/to/poetry /usr/local/bin/poetry

# Lancer en root en préservant toute les variables d'environnements
- sudo -E command

# VERSION FONCTIONNELLE ET AUTRES DÉPENDANCES
Python 3.9
- npm --version
9.2.0
- node -v
v18.19.0
- docker -v
Docker version 20.10.24+dfsg1, build 297e128
(sudo apt install docker.io)

# DOCKER
- sudo systemctl start docker
- sudo systemctl status docker
- sudo systemctl daemon-reload
- docker info
- docker system prune -a (permet de supprimer toute les images docker)

# GESTION DES SESSIONS
## voir les sessions
- screen -ls
ou
- sudo -E screen -ls (si vous avez lancé en root)
## éteindre une session :
- screen -X -S session_name quit

# VOIR LES PROCESSUS UTILISANTS UN PORT
- lsof -i :8000

#Régler les pbs avec docker (Permission denied):
- sudo usermod -aG docker bournon241
- sudo chmod 666 /var/run/docker.sock

# Résolutions
## Résoudre l'erreur suivante :
``Get "https://registry-1.docker.io/v2/": net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)``
Cette erreur a lieu lors de l'installation du projet sur le serveur de dev. Nous utilisons un proxy. Pour la plupart des commandes, écrire :
- export http_proxy="http://proxy.unicaen.fr:3128"
- export https_proxy="http://proxy.unicaen.fr:3128"
suffit et permet donc d'utiliser curl, pip, wget etc. 
Dans le cas de notre version de docker, nous devons spécifier les paramètres de proxy dans la configuration de docker.
- sudo systemctl edit docker.service
Nous pouvons ajouter les lignes suivantes sous [Service] :
- HTTP_PROXY="http://proxy.unicaen.fr:3128"
- HTTPS_PROXY="http://proxy.unicaen.fr:3128"
ou bien, si la ligne suivante existe, nous pouvons les spécifiés dans le fichier indiqué :
``EnvironmentFile=-/etc/default/docker``

### Les commandes à lancer ensuite :
Recharger la configuration des unités systemd. "Garantit que systemd dispose des dernières informations de configuration" :
- sudo systemctl daemon-reload
Redémarrer le service docker :
- sudo systemctl restart docker

à noter que dans le Dockerfile, les lignes suivantes devrons donc être ajouter si vous voulez installer des paquets :
ENV http_proxy="http://proxy.unicaen.fr:3128"
ENV https_proxy="http://proxy.unicaen.fr:3128"

## ERR98 Address already in use
- sudo systemctl restart docker
