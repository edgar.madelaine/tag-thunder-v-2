pour ajouter ou mmodifeir des route il faux modifer plusieur fichier


# explication generate 
- le phichier api/router/router.py, contient toute les route utiliset dans l'api. ici ils n'y a pas besuiont de modifier les route mais ils faux en ajouter si vous utiliser un autre type d'algorithme 

- le fichier api/router/service.py, definier l'entre a la sartye de chaqu'un des niveau du pipline 

- le fichier api/router/query.py, réalise lapelle a tout les query du pipeline. par défaux les query corespnonde a la fonction default_query difinie dans les fichier api/configurations/pipeline/XXX.py


# protocole de medification de route 

vous avez un Nom_Algo et en Module_Algo parmie les type suivent :(augmentation, cleaning, segmentation, extration, vocalisation)

dans la clace Module_AlgoBlocks qui ce trouve dans le fichier api/configurations/pipeline/Module_Algo.py
ajouter ce bloque de code 
```
Nom_Algo = Type8AlgoBlockConfig(
    name="Nom_Algo",
        enable=True,
        algorithm=pipeline.blocks.Module_Algo.Nom_Algo(),
        query=Module_AlgoBlockConfig.build_request_body()
)
```

sur la ligne ``algorithm=pipeline.blocks.Module_Algo.Nom_Algo()`` le Module_Algo et le Nom_Algo coresponde arespectivement dosier est fichier. dans pipeline/block . (si il ne sont pas crée suiver le protocole "nouveauxAlgo")

et pour ce qui est de `` query=Module_AlgoBlockConfig.build_request_bod()`` on doit retrouver tout les paramaitre nésésaire a l'agorithme Nom_Algo 
 
votre route vére votre algo et désomer ajpouter

## modification du réglage par défaut 
vous pouver modifier la query pas défaux pour cela deux solution: 

- modifiet la fonction default_query dans le fichier api/configurations/pipeline/XXX.py

- modifier dans api/router/query.py la class corespondent au Module_Algo pour apelter par défaut la nouvelle métode (cette métode et moi modulable)




# la suiste est pas fini
# protocole pour  ajouter une route 
## pour ajouter une nouvelle route dans router 
dans le fichier api/router/router.py suivre ce modelle 

```
@router.poste( 
Routes."/Non de la route/"
descripin = "la description de la route ",


response_class=fastapi.responses.JSONResponse 
response_model=schemas.Segmentation 


)
```
dans cette exemple il et posible de remplacer ``@router.poset()`` par un get() ``@router.get()``

le mieux pour cette ligne ``Routes."/Non de la route/"`` c'est d'ajouter le nom de la route dans la classe Route au début du fichier

les deux dernier ligne peuve estre optionelle. 
si vous utiliser  ``response_class=fastapi.responses.JSONResponse `` on peux changer de Response comme par exemple ``HTMLResponse`` ou d'autre compatible avec fastapi

## pour les query 
une foit que la route a été crée ils faux passer au query 

exemple: 

```
def Nom_Type(
        query: queries.Nom_TypeQuery
):
    return services.AlgorithmServices.Nom_Type(
        query.htmlp,
        query.algorithm.name,
        query.algorithm.parameters.dict()
    )

```


