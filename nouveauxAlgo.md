# protocole d’ajout d’algorithme

Ceci est un protocole pour ajouter des algorithmes sur le projet tag thunder.

## Première étape
Tous les algo du projet se trouve au niveau de pipeline/blocks

Vous trouverez les dossiers correspondant à chacun des module d’algorithmes utilisé (Vous pouvez ajouter un autre type d’algorithme en respectant le même schéma)


une fois le type d’algorithme choisi, ajouter votre algorithme en respectant :
- Les méthodes abstraite définie dans le fichier "_abstracte.py" 
- Respecter le type d’entrée de l’étage et le type de sortie


### Les type d’entrée et de sortie, de chacun des étages
Dans notre projet, le but est d’avoir en entrée du pipi, une URL ou de l’HTML, puis de me traiter plus que de l’html pp où appeler HTML augmenter

#### Version la plus récente

- url (ou HTML) > augmentation > HTMLP 
- HTMLP > cleaning > HTMLPP 
- HTMLPP > Segmentation, Extraction > HTMLPP 
- HTMLPP > Vocalisation > XXX.mp3 

#### Version ancienne (Accessman)
- url (ou HTML) > augmentation > HTMLP 
- HTMLP > cleaning > HTMLPP 
- HTMLPP > Segmentation > JSON 
- JSON > Extraction > JSON


À présent, votre algorithme est ajouté, il ne voit plus qu’à suivre le protocole de route









